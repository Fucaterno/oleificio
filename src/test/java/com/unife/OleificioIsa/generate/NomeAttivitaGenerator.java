/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.generate;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

public class NomeAttivitaGenerator extends Generator<String> {

    public NomeAttivitaGenerator() {
        super(String.class);
    }

    @Override
    public String generate(SourceOfRandomness sor, GenerationStatus gs) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 15; i++) {
            if(i<1){
                sb.append(sor.nextChar('A', 'Z'));
            }else{
                sb.append(sor.nextChar('A', 'Z'));
                sb.append(sor.nextChar('a','z'));
            }
        }

        return sb.toString();
    }

}