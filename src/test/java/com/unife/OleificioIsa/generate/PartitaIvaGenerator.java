/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.generate;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

public class PartitaIvaGenerator extends Generator<String> {

    public PartitaIvaGenerator() {
        super(String.class);
    }

    @Override
    public String generate(SourceOfRandomness sor, GenerationStatus gs) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 13; i++) {
            if(i<2){
                sb.append(sor.nextChar('A', 'Z'));
            }else{
                sb.append(sor.nextInt(0, 9));
            }
        }

        return sb.toString();
    }

}