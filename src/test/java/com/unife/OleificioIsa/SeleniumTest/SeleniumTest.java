/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.SeleniumTest;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gioacchino
*/

import com.unife.OleificioIsa.OleificioIsaApplication;
//import org.hibernate.dialect.Dialect;
//import org.hibernate.sql.Select;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = OleificioIsaApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SeleniumTest {

    private WebDriver driver;

    @BeforeClass
    public static void start() {
        final String webDriverPath = "C:\\Users\\XPS15\\Documents\\NetBeansProjects\\OleificioIsa\\chromedriver.exe";
        
        System.setProperty("webdriver.chrome.driver", webDriverPath);
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws InterruptedException {
        //Collegamento localhost
        driver.get("http://localhost:8080");
        
        
        //Visualizza Clienti
        driver.findElement(By.linkText("Clienti")).click();
        
        //Aggiungi primo cliente
        driver.findElement(By.linkText("Aggiungi cliente")).click();
        driver.findElement(By.name("codiceFiscale")).sendKeys("MNLGCH94S24Z133G");
        driver.findElement(By.name("cognome")).sendKeys("Monella");
        driver.findElement(By.name("nome")).sendKeys("Gioacchino");
        driver.findElement(By.name("telefono")).clear();
        driver.findElement(By.name("telefono")).sendKeys("3209042572");
        driver.findElement(By.id("Salva")).click();
        
        //Aggiungi secondo cliente
        driver.findElement(By.linkText("Aggiungi cliente")).click();
        driver.findElement(By.name("codiceFiscale")).sendKeys("RSSVCN70F21G823L");
        driver.findElement(By.name("cognome")).sendKeys("Rossi");
        driver.findElement(By.name("nome")).sendKeys("Vincenzo");
        driver.findElement(By.name("telefono")).clear();
        driver.findElement(By.name("telefono")).sendKeys("3339443857");
        driver.findElement(By.id("Salva")).click();
        
        //Aggiungi deposito
        driver.findElement(By.linkText("Deposita Olive")).click();
        Select dropdownCfCliente = new Select(driver.findElement(By.id("cfCliente")));
        dropdownCfCliente.selectByVisibleText("RSSVCN70F21G823L");
        driver.findElement(By.name("turno")).sendKeys("A1");
        driver.findElement(By.name("tipoOliva")).sendKeys("Ciarraffe");
        driver.findElement(By.name("ceste")).sendKeys("30-34");
        driver.findElement(By.name("peso")).clear();
        driver.findElement(By.name("peso")).sendKeys("502.55");
        driver.findElement(By.id("Salva")).click();

        //Acquista olio
        driver.findElement(By.linkText("Disponibilità olio")).click();
        Select dropdownCfClienteA = new Select(driver.findElement(By.id("cfClienteA")));
        dropdownCfClienteA.selectByVisibleText("RSSVCN70F21G823L");
        driver.findElement(By.name("tipoOliva")).sendKeys("Ciarraffe");
        driver.findElement(By.name("prezzoLt")).clear();
        driver.findElement(By.name("prezzoLt")).sendKeys("4.56");
        driver.findElement(By.name("litri")).clear();
        driver.findElement(By.name("litri")).sendKeys("100");
        driver.findElement(By.id("Salva")).click();
        
        //Registra prenotazione
        driver.findElement(By.linkText("Registra prenotazione")).click();
        Select dropdownTipoOliva = new Select(driver.findElement(By.id("tipologiaOliva")));
        dropdownTipoOliva.selectByVisibleText("Ciarraffe");
        driver.findElement(By.name("numeroPrenotazione")).sendKeys("123456");
        driver.findElement(By.name("partitaIva")).sendKeys("IT98765432122");
        driver.findElement(By.name("nomeAttivita")).sendKeys("La Barca");
        driver.findElement(By.name("prezzoLt")).clear();
        driver.findElement(By.name("prezzoLt")).sendKeys("7.50");
        driver.findElement(By.name("litri")).clear();
        driver.findElement(By.name("litri")).sendKeys("10");
        driver.findElement(By.name("tot")).clear();
        driver.findElement(By.name("tot")).sendKeys("75");
        driver.findElement(By.id("Salva")).click();        
        
        //Modifica primo cliente
        driver.navigate().to("http://localhost:8080/clienti/modifica/MNLGCH94S24Z133G");
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Gioacchino Calogero");
        driver.findElement(By.name("telefono")).clear();
        driver.findElement(By.name("telefono")).sendKeys("3399042272");
        driver.findElement(By.id("Salva")).click();
        
        //Modifica Deposito
        driver.navigate().to("http://localhost:8080/depositi/modifica/A1");
        
        driver.findElement(By.name("ceste")).clear();
        driver.findElement(By.name("ceste")).sendKeys("30-34-40");
        driver.findElement(By.name("peso")).clear();
        driver.findElement(By.name("peso")).sendKeys("735.55");
        driver.findElement(By.id("Salva")).click();
        
        //Modifica acquisto
        driver.navigate().to("http://localhost:8080/acquisti/modifica/Ciarraffe");
        driver.findElement(By.name("litri")).clear();
        driver.findElement(By.name("litri")).sendKeys("150");
        driver.findElement(By.id("Salva")).click();
        
        
        //Cancella deposito
        driver.navigate().to("http://localhost:8080/depositi/rimuovi/A1");
        driver.findElement(By.id("cancellaSi")).click();
        
        //Cancella prenotazione
        driver.navigate().to("http://localhost:8080/prenotazioni/rimuovi/123456");
        driver.findElement(By.id("cancellaSi")).click();
        
        //Cancella prenotazione
        driver.navigate().to("http://localhost:8080/clienti/rimuovi/MNLGCH94S24Z133G");
        driver.findElement(By.id("cancellaSi")).click();

    }
    
    @After
    public void tearDown() {
     driver.quit();
    }

}
