/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.test;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import com.unife.OleificioIsa.generate.CesteGenerator;
import com.unife.OleificioIsa.generate.InvalidCesteGenerator;
import com.unife.OleificioIsa.generate.InvalidTipoOlivaGenerator;
import com.unife.OleificioIsa.generate.InvalidTurnoGenerator;
import com.unife.OleificioIsa.generate.TipoOlivaGenerator;
import com.unife.OleificioIsa.generate.TurnoGenerator;
import com.unife.OleificioIsa.model.Deposito;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 *
 * @author Gioacchino
 */

@RunWith(JUnitQuickcheck.class)
public class DepositoTest {
    
    @BeforeClass
    public static void printName() {
        System.out.println("[Unit test] DepositoTest");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("[Unit test] DepositoTest-Fine");
    }
    
    /**
     * Viene testato l'inserimento di una stringa corrispondente al turno: 
     * @param valid  viene definita valida una stringa composta da una lettera maiuscola seguita da un numero intero(0-99)
     * @param invalid
     * tali stringhe vengono rispettivamente generate dalle classi "TurnoGenerator" ed
     * "InvalidTurnoGenerator".
     */
    @Property(trials = 50)
    public void testSetTurno(@From(InvalidTurnoGenerator.class) String invalid, @From(TurnoGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Deposito().setTurno(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Deposito().setTurno(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    /**
     * Viene testato l'inserimento di una stringa corrispondente al Tipo di oliva:  
     * @param valid  viene definita valida una stringa con max 100 caratteri
     * @param invalid
     * tali stringhe vengono rispettivamente generate dalle classi "TipoOlivaGenerator" ed
     * "InvalidTipoOlivaGenerator".
     */
    @Property(trials = 50)
    public void testSetTipoOliva(@From(InvalidTipoOlivaGenerator.class) String invalid, @From(TipoOlivaGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Deposito().setTipoOliva(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Deposito().setTipoOliva(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    /**
     * Viene testato l'inserimento di una stringa che indica le Ceste in cui vengono depositate le olive: 
     * @param valid  viene definita valida una stringa composta da numeri che identificano le ceste separate da "-" 
     *               Si possono inserire al MAX 4 ceste
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "CesteGenerator" ed
     * "InvalidCesteGenerator".
     */
    @Property(trials = 50)
    public void testSetCeste(@From(InvalidCesteGenerator.class) String invalid, @From(CesteGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Deposito().setCeste(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Deposito().setCeste(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    /**
     * Viene testato l'inserimento di un numero corrispondente al peso complessivo delle olive depositate:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Viene definito il range dei numeri ammissibili.
     */
    @Property(trials = 50)
    public void testSetPeso(@InRange(maxFloat = 0) float negativeId, @InRange(minFloat = (float) 0.1) float positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Deposito().setPeso(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Deposito().setPeso(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
}
