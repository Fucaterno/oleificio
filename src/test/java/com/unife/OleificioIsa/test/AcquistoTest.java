/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.test;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import com.unife.OleificioIsa.generate.InvalidTipoOlivaGenerator;
import com.unife.OleificioIsa.generate.TipoOlivaGenerator;
import com.unife.OleificioIsa.model.Acquisto;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 *
 * @author Gioacchino
 */

@RunWith(JUnitQuickcheck.class)
public class AcquistoTest {
    
    @BeforeClass
    public static void printName() {
        System.out.println("[Unit test] AcquistoTest");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("[Unit test] AcquistoTest-Fine");
    }
    
    /**
     * Viene testato l'inserimento di una stringa corrispondente al Tipo di oliva:  
     * @param valid  viene definita valida una stringa con max 100 caratteri
     * @param invalid
     * tali stringhe vengono rispettivamente generate dalle classi "TipoOlivaGenerator" ed
     * "InvalidTipoOlivaGenerator"
     */
    @Property(trials = 50)
    public void testSetTipoOliva(@From(InvalidTipoOlivaGenerator.class) String invalid, @From(TipoOlivaGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Acquisto().setTipoOliva(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Acquisto().setTipoOliva(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    
    /**
     * Viene testato l'inserimento di un numero corrispondente al prezzo/Lt:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Viene definito il range dei numeri ammissibili
     */
    @Property(trials = 50)
    public void testSetPrezzoLt(@InRange(maxFloat = 0) float negativeId, @InRange(minFloat = (float) 0.1) float positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Acquisto().setPrezzoLt(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Acquisto().setPrezzoLt(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    /**
     * Viene testato l'inserimento di un numero corrispondente al numero di litri acquistati:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Viene definito il range dei numeri ammissibili
     */
    @Property(trials = 50)
    public void testSetLitri(@InRange(maxFloat = 0) float negativeId, @InRange(minFloat = (float) 0.1) float positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Acquisto().setLitri(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Acquisto().setLitri(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }


}
