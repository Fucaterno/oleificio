/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.test;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import com.unife.OleificioIsa.generate.InvalidNPrenotazioneGenerator;
import com.unife.OleificioIsa.generate.InvalidNomeAttivitaGenerator;
import com.unife.OleificioIsa.generate.InvalidPartitaIvaGenerator;
import com.unife.OleificioIsa.generate.NPrenotazioneGenerator;
import com.unife.OleificioIsa.generate.NomeAttivitaGenerator;
import com.unife.OleificioIsa.generate.PartitaIvaGenerator;
import com.unife.OleificioIsa.model.Prenotazione;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 *
 * @author Gioacchino
 */

@RunWith(JUnitQuickcheck.class)
public class PrenotazioneTest {
    
    @BeforeClass
    public static void printName() {
        System.out.println("[Unit test] PrenotazioneTest");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("[Unit test] PrenotazioneTest-Fine");
    }
    
        /**
     * Viene testato l'inserimento della Partita IVA: 
     * @param valid  viene definita valida una stringa composta da due lettere seguite da 11 cifre
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "PartitaIvaGenerator" ed
     * "InvalidPartitaIvaGenerator".
     */
    @Property(trials = 50)
    public void testNumeroPrenotazione(@From(InvalidNPrenotazioneGenerator.class) String invalid, @From(NPrenotazioneGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Prenotazione().setNumeroPrenotazione(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Prenotazione().setNumeroPrenotazione(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    /**
     * Viene testato l'inserimento della Partita IVA: 
     * @param valid  viene definita valida una stringa composta da due lettere seguite da 11 cifre
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "PartitaIvaGenerator" ed
     * "InvalidPartitaIvaGenerator".
     */
    @Property(trials = 50)
    public void testPartitaIva(@From(InvalidPartitaIvaGenerator.class) String invalid, @From(PartitaIvaGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Prenotazione().setPartitaIva(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Prenotazione().setPartitaIva(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    /**
     * Viene testato l'inserimento del nome dell'Attività che effettua la prenotazione: 
     * @param valid  viene definita valida una stringa composta da 30 caratteri includendo lettere maiuscole e non
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "NomeAttivitaGenerator" ed
     * "InvalidNomeAttivitaGenerator".
     */
    @Property(trials = 50)
    public void testNomeAttivita(@From(InvalidNomeAttivitaGenerator.class) String invalid, @From(NomeAttivitaGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Prenotazione().setNomeAttivita(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Prenotazione().setNomeAttivita(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }    
 
    /**
     * Viene testato l'inserimento di un numero corrispondente al prezzo/Lt:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Quindi viene definito il range dei numeri che possono essere ammessi.
     */
    @Property(trials = 50)
    public void testSetPrezzoLt(@InRange(maxFloat = 0) float negativeId, @InRange(minFloat = (float) 0.1) float positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Prenotazione().setPrezzoLt(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Prenotazione().setPrezzoLt(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    /**
     * Viene testato l'inserimento di un numero corrisponente ai Litri:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Quindi viene definito il range dei numeri che possono essere ammessi.
     */
    @Property(trials = 50)
    public void testSetLitri(@InRange(maxFloat = 0) float negativeId, @InRange(minFloat = (float) 0.1) float positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Prenotazione().setLitri(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Prenotazione().setLitri(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    /**
     * Viene testato l'inserimento di un numero corrispondente al prezzo Totale:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Quindi viene definito il range dei numeri che possono essere ammessi.
     */
    @Property(trials = 50)
    public void testSetTot(@InRange(maxFloat = 0) float negativeId, @InRange(minFloat = (float) 0.1) float positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Prenotazione().setTot(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Prenotazione().setTot(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
}
