/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.test;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import com.unife.OleificioIsa.generate.CodiceFiscaleGenerator;
import com.unife.OleificioIsa.generate.InvalidCodiceFiscaleGenerator;
import com.unife.OleificioIsa.generate.InvalidNomeGenerator;
import com.unife.OleificioIsa.generate.NomeGenerator;
import com.unife.OleificioIsa.model.Cliente;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 *
 * @author Gioacchino  
 */

@RunWith(JUnitQuickcheck.class)
public class ClienteTest {
    
    @BeforeClass
    public static void printName() {
        System.out.println("[Unit test] ClientiTest");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("[Unit test] ClientiTest-Fine");
    }
    
    /**
     * Viene testato l'inserimento di una stringa corrispondente al Nome: 
     * @param valid  viene definita valida una stringa con max 40 caratteri avente la prima lettera maiuscola
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "NomeGenerator" ed
     * "InvalidNomeGenerator".
     */
    @Property(trials = 50)
    public void testSetNome(@From(InvalidNomeGenerator.class) String invalid, @From(NomeGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Cliente().setNome(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Cliente().setNome(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    /**
     *Viene testato l'inserimento di una stringa corrispondente al Cognome: 
     * @param valid  viene definita valida una stringa con max 40 caratteri avente la prima lettera maiuscola
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "NomeGenerator" ed
     * "InvalidNomeGenerator".
     */
    @Property(trials = 50)
    public void testSetCognome(@From(InvalidNomeGenerator.class) String invalid, @From(NomeGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Cliente().setCognome(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Cliente().setCognome(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    /**
     * Viene testato l'inserimento di una stringa: 
     * @param valid  viene definita valida una stringa composta da lettere maiuscole e numeri, con max 16 caratteri
     * @param invalid 
     * tali stringhe vengono rispettivamente generate dalle classi "CodiceFiscaleGenerator" ed
     * "InvalidCodiceFiscaleGenerator".
     */
    @Property(trials = 50)
    public void testSetCodiceFiscale(@From(InvalidCodiceFiscaleGenerator.class) String invalid, @From(CodiceFiscaleGenerator.class) String valid) {
        IllegalArgumentException e1 = null;
        try {
            new Cliente().setCodiceFiscale(invalid);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Cliente().setCodiceFiscale(valid);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
    /**
     * Viene testato l'inserimento di un numero:
     * @param negativeId  numero negativo e quindi non ammissibile
     * @param positiveId  minimo numero ammissibile
     * Il numero di telefono deve essere maggiore di 0.
     */
    @Property(trials = 50)
    public void testSetTelefono(@InRange(maxLong = 0) long negativeId, @InRange(minLong = 1) long positiveId) {
        IllegalArgumentException e1 = null;
        try {
            new Cliente().setTelefono(negativeId);
        } catch (IllegalArgumentException ex) {
            e1 = ex;
        }
        assertNotNull(e1);

        IllegalArgumentException e2 = null;
        try {
            new Cliente().setTelefono(positiveId);
        } catch (IllegalArgumentException ex) {
            e2 = ex;
        }
        assertNull(e2);
    }
    
}
