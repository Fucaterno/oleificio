/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.controller;

import com.unife.OleificioIsa.model.Acquisto;
import com.unife.OleificioIsa.model.Cliente;
import com.unife.OleificioIsa.model.Deposito;
import com.unife.OleificioIsa.model.Prenotazione;
import com.unife.OleificioIsa.service.AcquistoService;
import com.unife.OleificioIsa.service.ClienteService;
import com.unife.OleificioIsa.service.DepositoService;
import com.unife.OleificioIsa.service.PrenotazioneService;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Gioacchino
 */
@Controller
public class OleificioIsaController {
 //______________CLIENTE_____________________  
    @Autowired
    private ClienteService clienteService;
    
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView home(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("home");
        return mav;
    }
    
    @RequestMapping(value="/clienti", method=RequestMethod.GET)
    public ModelAndView visualizzaClienti(){
        ModelAndView mav=new ModelAndView();
        List<Cliente> listaClienti = clienteService.getAll();
        mav.addObject("listaClienti", listaClienti);
        mav.setViewName("gestisci_clienti");
        return mav;
    }
    
    @RequestMapping(value="/clienti/aggiungi", method=RequestMethod.GET)
    public ModelAndView aggiungiClienti(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("aggiungi_cliente");
        mav.addObject("clienti", new Cliente());
        return mav;
    }
    
    @RequestMapping(value="/clienti/salva", method=RequestMethod.POST)
    public ModelAndView salvaCliente(@Valid Cliente c , BindingResult bindingResult){
        ModelAndView mav=new ModelAndView();
        clienteService.saveCliente(c);
        mav.addObject("clienti", new Cliente());
        //mav.setViewName("gestisci_clienti");
        //return mav;
        return new ModelAndView("redirect:/clienti");
    }
    
    @RequestMapping(value="/clienti/modifica/{clienteId}", method=RequestMethod.GET)
    public ModelAndView updateCliente(@PathVariable String clienteId){
        ModelAndView mav=new ModelAndView();

        mav.setViewName("modifica_cliente");
        mav.addObject("clienti", clienteService.findById(clienteId));
        return mav;
    }
    
    @RequestMapping (value = "/clienti/modifica",method = RequestMethod.POST)
    public ModelAndView doUpdateCliente(@Valid Cliente c, BindingResult bindingResult)
    {
        System.out.println(c.getCodiceFiscale());
        clienteService.saveCliente(c);
        return new ModelAndView("redirect:/clienti");
    }
    @RequestMapping (value = "/clienti/rimuovi/{clienteId}",method = RequestMethod.GET)
    public ModelAndView eliminaCliente(@PathVariable String clienteId)
    {
        ModelAndView mav = new ModelAndView();
        Optional<Cliente> clientiFound = clienteService.findById(clienteId);
        
        mav.addObject("clienti", clientiFound.get());
        mav.setViewName("elimina_cliente");
        return mav;
    }
    
    @RequestMapping (value = "/clienti/rimuovi",method = RequestMethod.POST)
    public ModelAndView confermaEliminazioneC(@Valid Cliente c, BindingResult bindingResult)
    {
        System.out.println(c.getCodiceFiscale());
        clienteService.deleteCliente(c.getCodiceFiscale());
        return new ModelAndView("redirect:/clienti");
    }
    @RequestMapping(value="/clienti/annulla", method=RequestMethod.GET)
    public ModelAndView annullaEliminazioneC(){
        //ModelAndView mav=new ModelAndView();
        return new ModelAndView("redirect:/clienti");
    }

//_________________DEPOSITO________________________________
    @Autowired
    private DepositoService depositoService;
    @RequestMapping(value="/depositi", method=RequestMethod.GET)
    public ModelAndView aggiungiDeposito(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("gestisci_depositi");
        List<Deposito> listaDepositi = depositoService.getAll();
        mav.addObject("listaDepositi", listaDepositi);        
        List<Cliente> listaClienti = clienteService.getAll();
        mav.addObject("listaClienti", listaClienti);
        mav.addObject("depositi", new Deposito());
        return mav;
    }
    @RequestMapping(value="/depositi/salva", method=RequestMethod.POST)
    public ModelAndView salvaDepositi(@Valid Deposito d , BindingResult bindingResult){
        ModelAndView mav=new ModelAndView();
        depositoService.saveDeposito(d);
        mav.addObject("depositi", new Deposito());
        List<Deposito> listaDepositi = depositoService.getAll();
        mav.addObject("listaDepositi", listaDepositi);
        //mav.setViewName("gestisci_clienti");
        //return mav;
        return new ModelAndView("redirect:/depositi");
    }
    
    @RequestMapping(value="/depositi/modifica/{depositoId}", method=RequestMethod.GET)
    public ModelAndView updateDeposito(@PathVariable String depositoId){
        ModelAndView mav=new ModelAndView();

        mav.setViewName("modifica_deposito");
        mav.addObject("depositi", depositoService.findById(depositoId));
        return mav;
    }
    
    @RequestMapping (value = "/depositi/modifica",method = RequestMethod.POST)
    public ModelAndView doUpdateDeposito(@Valid Deposito d, BindingResult bindingResult)
    {
        System.out.println(d.getTurno());
        depositoService.saveDeposito(d);
        return new ModelAndView("redirect:/depositi");
    }
    
    @RequestMapping (value = "/depositi/rimuovi/{depositoId}",method = RequestMethod.GET)
    public ModelAndView eliminaDeposito(@PathVariable String depositoId)
    {
        ModelAndView mav = new ModelAndView();
        Optional<Deposito> depositiFound = depositoService.findById(depositoId);
        
        mav.addObject("depositi", depositiFound.get());
        mav.setViewName("elimina_deposito");
        return mav;
    }
    
    @RequestMapping (value = "/depositi/rimuovi",method = RequestMethod.POST)
    public ModelAndView confermaEliminazione(@Valid Deposito d, BindingResult bindingResult)
    {
        System.out.println(d.getTurno());
        depositoService.deleteDeposito(d.getTurno());
        return new ModelAndView("redirect:/depositi");
    }
    @RequestMapping(value="/depositi/annulla", method=RequestMethod.GET)
    public ModelAndView annullaEliminazione(){
        //ModelAndView mav=new ModelAndView();
        return new ModelAndView("redirect:/depositi");
    }
    
//___________________ACQUISTO_____________________________________
    @Autowired
    private AcquistoService acquistoService;
    @RequestMapping(value="/acquisti", method=RequestMethod.GET)
    public ModelAndView aggiungiAcquisto(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("gestisci_acquisti");
        List<Acquisto> listaAcquisti = acquistoService.getAll();
        mav.addObject("listaAcquisti", listaAcquisti);        
        List<Cliente> listaClienti = clienteService.getAll();
        mav.addObject("listaClienti", listaClienti);
        mav.addObject("acquisti", new Acquisto());
        return mav;
    }
    @RequestMapping(value="/acquisti/salva", method=RequestMethod.POST)
    public ModelAndView salvaAcquisti(@Valid Acquisto a , BindingResult bindingResult){
        ModelAndView mav=new ModelAndView();
        acquistoService.saveAcquisto(a);
        mav.addObject("acquisti", new Acquisto());
        List<Acquisto> listaAcquisti = acquistoService.getAll();
        mav.addObject("listaAcquisti", listaAcquisti);
        //mav.setViewName("gestisci_clienti");
        //return mav;
        return new ModelAndView("redirect:/acquisti");
    }
    @RequestMapping(value="/acquisti/modifica/{acquistoId}", method=RequestMethod.GET)
    public ModelAndView updateAcquisto(@PathVariable String acquistoId){
        ModelAndView mav=new ModelAndView();

        mav.setViewName("modifica_acquisto");
        mav.addObject("acquisti", acquistoService.findById(acquistoId));
        return mav;
    }
    
    @RequestMapping (value = "/acquisti/modifica",method = RequestMethod.POST)
    public ModelAndView doUpdateAcquisto(@Valid Acquisto acquisto, BindingResult bindingResult)
    {
        System.out.println(acquisto.getTipoOliva());
        acquistoService.saveAcquisto(acquisto);
        return new ModelAndView("redirect:/acquisti");
    }

//____________________PRENOTAZIONI_________________________________________
    @Autowired
    private PrenotazioneService prenotazioneService;
    @RequestMapping(value="/prenotazioni", method=RequestMethod.GET)
    public ModelAndView aggiungiPrenotazione(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("gestisci_prenotazioni");
        List<Prenotazione> listaPrenotazioni = prenotazioneService.getAll();
        mav.addObject("listaPrenotazioni", listaPrenotazioni);        
        List<Acquisto> listaAcquisti = acquistoService.getAll();
        mav.addObject("listaAcquisti", listaAcquisti);
        mav.addObject("prenotazioni", new Prenotazione());
        return mav;
    }
    @RequestMapping(value="/prenotazioni/salva", method=RequestMethod.POST)
    public ModelAndView salvaPrenotazione(@Valid Prenotazione p , BindingResult bindingResult){
        ModelAndView mav=new ModelAndView();
        prenotazioneService.savePrenotazione(p);
        mav.addObject("prenotazioni", new Prenotazione());
        List<Prenotazione> listaPrenotazioni = prenotazioneService.getAll();
        mav.addObject("listaPrenotazioni", listaPrenotazioni);
        //mav.setViewName("gestisci_clienti");
        //return mav;
        return new ModelAndView("redirect:/prenotazioni");
    }
    @RequestMapping (value = "/prenotazioni/rimuovi/{prenotazioneId}",method = RequestMethod.GET)
    public ModelAndView eliminaPrenotazione(@PathVariable String prenotazioneId)
    {
        ModelAndView mav = new ModelAndView();
        Optional<Prenotazione> prenotazioniFound = prenotazioneService.findById(prenotazioneId);
        
        mav.addObject("prenotazioni", prenotazioniFound.get());
        mav.setViewName("elimina_prenotazione");
        return mav;
    }
    
    @RequestMapping (value = "/prenotazioni/rimuovi",method = RequestMethod.POST)
    public ModelAndView confermaEliminazione(@Valid Prenotazione p, BindingResult bindingResult)
    {
        System.out.println(p.getNumeroPrenotazione());
        prenotazioneService.deletePrenotazione(p.getNumeroPrenotazione());
        return new ModelAndView("redirect:/prenotazioni");
    }
    @RequestMapping(value="/prenotazioni/annulla", method=RequestMethod.GET)
    public ModelAndView annullaEliminazioneP(){
        //ModelAndView mav=new ModelAndView();
        return new ModelAndView("redirect:/prenotazioni");
    }
    
    
}
