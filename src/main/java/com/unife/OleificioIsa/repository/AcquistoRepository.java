/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.repository;

import com.unife.OleificioIsa.model.Acquisto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Gioacchino
 */
public interface AcquistoRepository extends JpaRepository<Acquisto, String> {
    
}
