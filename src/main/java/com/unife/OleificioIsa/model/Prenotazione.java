/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * L'entità serve a registrare le prenotazioni di Olio effettuate dalle varie attività.
 * Si può selezionare la tipologia di olive tra quelle disponibili.
 * Vengono salvati:
 * -Numero di prenotazione
 * -Partita IVA;
 * -Nome attività;
 * -Prezzo al Litro ;
 * -numero di Litri prenotati;
 * -Tot 
 * @author Gioacchino
 */
@Entity
@Table(name="prenotazioni")
public class Prenotazione implements Serializable {
    private String numeroPrenotazione;
    private String partitaIva;
    private String nomeAttivita;
    private float prezzoLt;
    private float litri;
    private float tot;

    
    private Acquisto acquisti;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tipo_oliva")
    /**
     * @return the acquisti
     */
    public Acquisto getAcquisti() {
        return acquisti;
    }

    /**
     * @param acquisti the acquisti to set
     */
    public void setAcquisti(Acquisto acquisti) {
        this.acquisti = acquisti;
    }
    
    @Id
    /**
     * @return the numeroPrenotazione
     */
    public String getNumeroPrenotazione() {
        return numeroPrenotazione;
    }

    /**
     * @param numeroPrenotazione the numeroPrenotazione to set
     */
    public void setNumeroPrenotazione(String numeroPrenotazione) {
        if(numeroPrenotazione.length()>6)
            throw new IllegalArgumentException("Il numero di prenotazione deve avere una lunghezza massima di 6 caratteri");
        this.numeroPrenotazione = numeroPrenotazione;
    }

    
    /**
     * @return the partitaIva
     */
    
    public String getPartitaIva() {
        return partitaIva;
    }

    /**
     * @param partitaIva the partitaIva to set
     */
    public void setPartitaIva(String partitaIva) {
        if(partitaIva.length()<13 || partitaIva.length()>15)
            throw new IllegalArgumentException("La partita IVA deve essere composta da massimo 14 caratteri");
        this.partitaIva = partitaIva;
    }

    /**
     * @return the prezzoLt
     */
    public float getPrezzoLt() {
        return prezzoLt;
    }

    /**
     * @param prezzoLt the prezzoLt to set
     */
    public void setPrezzoLt(float prezzoLt) {
        if(prezzoLt<=0)
            throw new IllegalArgumentException("Il prezzo deve essere maggiore di 0");
        this.prezzoLt = prezzoLt;
    }

    /**
     * @return the litri
     */
    public float getLitri() {
        return litri;
    }

    /**
     * @param litri the litri to set
     */
    public void setLitri(float litri) {
        if(litri<=0)
            throw new IllegalArgumentException("Il valore litri deve essere maggiore di 0");
        this.litri = litri;
    }

    /**
     * @return the tot
     */
    public float getTot() {
        return tot;
    }

    /**
     * @param tot the tot to set
     */
    public void setTot(float tot) {
         if(tot<=0)
            throw new IllegalArgumentException("Il totale deve essere maggiore di 0");
        this.tot = tot;
    }

    /**
     * @return the nomeAttivita
     */
    public String getNomeAttivita() {
        return nomeAttivita;
    }

    /**
     * @param nomeAttivita the nomeAttivita to set
     */
    public void setNomeAttivita(String nomeAttivita) {
        if(nomeAttivita.length()<1 || nomeAttivita.length()>50)
            throw new IllegalArgumentException("Il nome dell'attività deve essere composto da più di un carattere");
        this.nomeAttivita = nomeAttivita;
    }


    
}
