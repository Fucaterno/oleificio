/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * L'entità serve a registrare i vari depositi effettuati dai clienti.
 * Vengono salvati:
 * -Il turno che identifica il deposito;
 * -Il tipo di oliva;
 * -Le ceste in cui vengono depositate le olive;
 * -Il peso complessivo delle olive depositate;
 * Tenendo traccia del CF Cliente che effettua il deposito
 * @author Gioacchino
 */
@Entity
@Table(name="depositi")
public class Deposito implements Serializable {
    private String turno;
    private String tipoOliva;
    private String ceste;
    private float peso;
    
    private Cliente clienti;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cf_cliente")

    /**
     * @return the clienti
     */
    public Cliente getClienti() {
        return clienti;
    }

    /**
     * @param clienti the clienti to set
     */
    public void setClienti(Cliente clienti) {
        this.clienti = clienti;
    }    

    /**
     * @return the turno
     */
    @Id
    public String getTurno() {
        return turno;
    }

    /**
     * @param turno the turno to set
     */
    public void setTurno(String turno) {
        if(turno.length()>3)
            throw new IllegalArgumentException("Il turno deve essere composto da una lettera seguita da due cifre");
        this.turno = turno;
    }

    /**
     * @return the tipoOliva
     */
    public String getTipoOliva() {
        return tipoOliva;
    }

    /**
     * @param tipoOliva the tipoOliva to set
     */
    public void setTipoOliva(String tipoOliva) {
        if(tipoOliva.length()<=0 || tipoOliva.length()>100)
            throw new IllegalArgumentException("Il tipo di oliva deve essere composto da 1-100 caratteri");
        this.tipoOliva = tipoOliva;
    }

    /**
     * @return the ceste
     */
    public String getCeste() {
        return ceste;
    }

    /**
     * @param ceste the ceste to set
     */
    public void setCeste(String ceste) {
        if(ceste.length()<1 || ceste.length()>15)
            throw new IllegalArgumentException("Inserire almeno una cesta, massimo 5 ceste consentite");
        this.ceste = ceste;
    }

    /**
     * @return the peso
     */
    public float getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(float peso) {
        if(peso<=0)
            throw new IllegalArgumentException("Il peso deve essere maggiore di 0");
        this.peso = peso;
    }

    
    
}
