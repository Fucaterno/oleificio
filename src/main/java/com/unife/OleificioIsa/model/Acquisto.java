/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * L'entità viene utilizzata per registrare l'Olio acquistato dall'oleificio, indicando:
 * -Tipo di Oliva;
 * -Prezzo di acquisto;
 * -Litri Acquistati;
 * Indica la disponibilità dell'olio tenendo traccia del CF del fornitore
 * @author Gioacchino
 */
@Entity
@Table(name="acquisti")
public class Acquisto implements Serializable {
    private String tipoOliva;
    private float prezzoLt;
    private float litri;


    private Cliente clienti;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cf_cliente")
    /**
     * @return the clienti
     */
    public Cliente getClienti() {
        return clienti;
    }

    /**
     * @param clienti the clienti to set
     */
    public void setClienti(Cliente clienti) {
        this.clienti = clienti;
    }
    
    
    private Set<Prenotazione> prenotazioni;
    @OneToMany(mappedBy="acquisti", cascade=CascadeType.ALL, orphanRemoval=false)
    /**
     * @return the prenotazioni
     */
    public Set<Prenotazione> getPrenotazioni() {
        return prenotazioni;
    }

    /**
     * @param prenotazioni the prenotazioni to set
     */
    public void setPrenotazioni(Set<Prenotazione> prenotazioni) {
        this.prenotazioni = prenotazioni;
    }


    @Id
    /**
     * @return the tipoOliva
     */
    public String getTipoOliva() {
        return tipoOliva;
    }

    /**
     * @param tipoOliva the tipoOliva to set
     */
    public void setTipoOliva(String tipoOliva) {
        if(tipoOliva.length()<0 || tipoOliva.length()>100)
            throw new IllegalArgumentException("Il tipo di oliva deve essere composto da 1-100 caratteri");
        this.tipoOliva = tipoOliva;
    }

    /**
     * @return the prezzoLt
     */
    public float getPrezzoLt() {
        return prezzoLt;
    }

    /**
     * @param prezzoLt the prezzoLt to set
     */
    public void setPrezzoLt(float prezzoLt) {
        if(prezzoLt<=0)
            throw new IllegalArgumentException("Il prezzo deve essere maggiore di 0");
        this.prezzoLt = prezzoLt;
    }

    /**
     * @return the litri
     */
    public float getLitri() {
        return litri;
    }

    /**
     * @param litri the litri to set
     */
    public void setLitri(float litri) {
        if(litri<=0)
            throw new IllegalArgumentException("Il valore litri deve essere maggiore di 0");
        this.litri = litri;
    }

    




}
