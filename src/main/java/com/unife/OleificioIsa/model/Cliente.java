/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * L'entità viene utilizzata per registrare i clienti dell'oleificio, ovvero 
 * coloro che depositano le olive e ne richiedono il servizio di macinazione.
 * Vengono salvati:
 * -Codice Fiscale;
 * -Nome;
 * -Cognome;
 * -Telefono;
 * @author Gioacchino
 */
@Entity
@Table(name="clienti")
public class Cliente implements Serializable {
    private String codiceFiscale;
    private String nome;
    private String cognome;
    private long   telefono;

    
    public Set<Deposito> depositi;
    @OneToMany(mappedBy="clienti", cascade=CascadeType.ALL, orphanRemoval=false)
    /**
     * @return the depositi
     */
    public Set<Deposito> getDepositi() {
        return depositi;
    }

    /**
     * @param depositi the depositi to set
     */
    public void setDepositi(Set<Deposito> depositi) {
        this.depositi = depositi;
    }
    
    private Set<Acquisto> acquisti;
    @OneToMany(mappedBy="clienti", cascade=CascadeType.ALL, orphanRemoval=false)
   
        /**
     * @return the acquisti
     */
    public Set<Acquisto> getAcquisti() {
        return acquisti;
    }

    /**
     * @param acquisti the acquisti to set
     */
    public void setAcquisti(Set<Acquisto> acquisti) {
        this.acquisti = acquisti;
    }
    /**
     * @return the codiceFiscale
     */
    @Id
    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    /**
     * @param codiceFiscale the codiceFiscale to set
     */
    public void setCodiceFiscale(String codiceFiscale) {
        if(codiceFiscale.length()<16 || codiceFiscale.length()>16)
            throw new IllegalArgumentException("Il codice fiscale deve avere lunghezza pari a 16");
        this.codiceFiscale = codiceFiscale;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        if(nome.length()>50)
            throw new IllegalArgumentException("Massima lunghezza consentita per il Nome è 50");
        this.nome = nome;
    }

    /**
     * @return the cognome
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * @param cognome the cognome to set
     */
    public void setCognome(String cognome) {
        if(cognome.length()>50)
            throw new IllegalArgumentException("Massima lunghezza consentita per il Cognome è 50");
        this.cognome = cognome;
    }

    /**
     * @return the telefono
     */
    public long getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(long telefono) {
        if(telefono<=0)
            throw new IllegalArgumentException("Il telefono deve essere maggiore di 0");
        this.telefono = telefono;
    }




}
