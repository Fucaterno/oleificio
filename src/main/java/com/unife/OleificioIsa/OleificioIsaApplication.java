package com.unife.OleificioIsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OleificioIsaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OleificioIsaApplication.class, args);
	}

}
