/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Acquisto;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Gioacchino
 */
public interface AcquistoService {
    public void saveAcquisto(Acquisto a);
    public List<Acquisto> getAll();

    public Optional<Acquisto> findById(String acquistoId);

    public void deleteAcquisto(String acquistoId);
    
}
