/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Prenotazione;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Gioacchino
 */
public interface PrenotazioneService {
    public void savePrenotazione(Prenotazione p);
    public List<Prenotazione> getAll();

    public Optional<Prenotazione> findById(String prenotazioneId);

    public void deletePrenotazione(String prenotazioneId);

}
