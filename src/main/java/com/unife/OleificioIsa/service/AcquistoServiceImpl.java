/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Acquisto;
import com.unife.OleificioIsa.repository.AcquistoRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gioacchino
 */
@Service("acquistoService")
public class AcquistoServiceImpl implements AcquistoService {
    @Autowired
    private AcquistoRepository acquistoRepository;
    @Override
    public void saveAcquisto(Acquisto a) {
        acquistoRepository.save(a);
    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Acquisto> getAll() {
        return acquistoRepository.findAll();
    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Acquisto> findById(String acquistoId) {
        return acquistoRepository.findById(acquistoId);
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAcquisto(String acquistoId) {
        acquistoRepository.deleteById(acquistoId);
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
