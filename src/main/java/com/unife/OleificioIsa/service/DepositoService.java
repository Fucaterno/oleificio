/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Deposito;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Gioacchino
 */
public interface DepositoService {
    public void saveDeposito(Deposito d);
    public List<Deposito> getAll();

    public Optional<Deposito> findById(String depositoId);

    public void deleteDeposito(String depositoId);
}
