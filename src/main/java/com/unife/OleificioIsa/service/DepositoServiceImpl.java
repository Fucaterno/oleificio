/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Deposito;
import com.unife.OleificioIsa.repository.DepositoRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gioacchino
 */
@Service("depositoService")
public class DepositoServiceImpl implements DepositoService{
    @Autowired
    private DepositoRepository depositoRepository;
    
    @Override
    public void saveDeposito(Deposito d) {
        depositoRepository.save(d);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Deposito> getAll() {
        return depositoRepository.findAll();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Deposito> findById(String depositoId) {
        return depositoRepository.findById(depositoId);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteDeposito(String depositoId) {
        depositoRepository.deleteById(depositoId);

//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
