/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Cliente;
import com.unife.OleificioIsa.repository.ClienteRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gioacchino
 */
@Service("clienteService")
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;
    
    @Override
    public void saveCliente(Cliente c) {
        clienteRepository.save(c);
    }

    @Override
    public List<Cliente> getAll() {
        return clienteRepository.findAll();
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Cliente> findById(String clienteID) {
        return clienteRepository.findById(clienteID);
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteCliente(String codiceFiscale) {
        clienteRepository.deleteById(codiceFiscale);
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
