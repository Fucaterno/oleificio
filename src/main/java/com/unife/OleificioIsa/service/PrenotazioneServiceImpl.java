/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Prenotazione;
import com.unife.OleificioIsa.repository.PrenotazioneRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gioacchino
 */
@Service("prenotazioneService")
public class PrenotazioneServiceImpl implements PrenotazioneService{
    @Autowired
    private PrenotazioneRepository prenotazioneRepository;
    
    @Override
    public void savePrenotazione(Prenotazione p) {
        prenotazioneRepository.save(p);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Prenotazione> getAll() {
        return prenotazioneRepository.findAll();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Prenotazione> findById(String prenotazioneId) {
        return prenotazioneRepository.findById(prenotazioneId);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletePrenotazione(String prenotazioneId) {
        prenotazioneRepository.deleteById(prenotazioneId);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
