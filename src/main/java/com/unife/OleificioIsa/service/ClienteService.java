/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unife.OleificioIsa.service;

import com.unife.OleificioIsa.model.Cliente;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Gioacchino
 */
public interface ClienteService {
    public void saveCliente(Cliente c);
    public List<Cliente> getAll();

    public Optional<Cliente> findById(String clienteId);

    public void deleteCliente(String codiceFiscale);
}
